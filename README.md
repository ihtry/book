# 图书管理

#### 介绍
基于jsp+servlet+jdbc图书管理

#### 软件架构
前端：Mdui+jQuery+jsp
后端：servlet+jdbc
开发环境：idea 2021.3+MySQL 5.6

#### 安装教程
1. 直接导入到idea
2. 配置com.ihtry.util.JdbcTool 中的数据库信息
3. 导入bookos.sql文件到数据库
#### 线上地址

http://book.ihtry.com

#### 作用
本项目为本人期末作品，有很多不足，还请见谅

