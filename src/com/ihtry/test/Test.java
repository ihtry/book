package com.ihtry.test;

import com.ihtry.util.JdbcTool;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Test {
    public static void main(String[] args) {
        String uploadPath = "C:\\Users\\Joker\\Desktop\\java\\out\\artifacts\\java\\static\\upload\\";
        String filePath = getFilePath(29);
        System.out.println(filePath);
        String name = StringUtils.substringAfterLast(filePath, "static/upload/");
        boolean delete = new File(uploadPath + File.separator + name).delete();
        System.out.println(delete);
    }

    public static String getFilePath(Integer id) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JdbcTool jdbcTool = new JdbcTool();
        String sql = "select * from books where id=?";
        String img = "";
        try {
            conn = jdbcTool.getConn();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                img = rs.getString("img");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            jdbcTool.release(conn, ps, rs);
        }

        return img;
    }
}
