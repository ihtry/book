package com.ihtry.servlet;

import com.ihtry.dao.BookDao;
import com.ihtry.entity.Book;
import com.ihtry.util.FileTool;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import java.io.File;
import java.io.IOException;


import java.util.List;

//文件上传
@MultipartConfig
@WebServlet("/book")
public class BookServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BookDao bookDao = new BookDao();
        FileTool fileTool = new FileTool();
        String uploadPath = req.getServletContext().getRealPath("static/upload");
        String method = req.getParameter("method");
        String id = null;
        if (method == null) {
            method = "index";
        }
        switch (method) {
            case "index":
                List<Book> books = bookDao.getBooks();
                req.setAttribute("books", books);
                req.getRequestDispatcher("home.jsp").forward(req, resp);
                break;
            case "list":
                List<Book> books1 = bookDao.getBooks();
                req.setAttribute("books", books1);
                req.getRequestDispatcher("list.jsp").forward(req, resp);
                break;
            case "edit":
                id = req.getParameter("id");
                Book book = bookDao.queryId(Integer.parseInt(id));
                req.setAttribute("book", book);
                req.getRequestDispatcher("edit.jsp").forward(req, resp);
                break;
            case "delete":
                //根据id删除
                id = req.getParameter("id");
                //删除对应的封面图
                String filePath = bookDao.getFilePath(Integer.parseInt(id));
                fileTool.deleteFile(uploadPath, Integer.parseInt(id));
                //删除数据库
                bookDao.delete(Integer.parseInt(id));
                resp.sendRedirect("book");
                break;
            default:
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //文件上传路径
        String uploadPath = req.getServletContext().getRealPath("/static/upload/");
        FileTool fileTool = new FileTool();
        //跨域解决
        resp.setHeader("Access-Control-Allow-Origin", "*");
        //创捷对象
        BookDao bookDao = new BookDao();
        //响应设置
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        req.setCharacterEncoding("utf-8");
        //获取参数
        String id = null;
        String name = null;
        String author = null;
        String express = null;
        String money = null;
        Part part = null;
        String method = req.getParameter("method");
        switch (method) {
            case "add":
                //获取参数
                name = req.getParameter("bookname");
                author = req.getParameter("author");
                express = req.getParameter("express");
                money = req.getParameter("money");
                part = req.getPart("img");
                //处理图片并且转换成路径
                String fileName = fileTool.getFileName(part);
                String fileExt = fileTool.getFileExt(fileName);
                //写入文件
                part.write(uploadPath + File.separator + fileName);
                //重命名
                File file = new File(uploadPath + File.separator + fileName);
                String newFileName = fileTool.getRandomString() + "." + fileExt;
                file.renameTo(new File(uploadPath + File.separator + newFileName));
                //将重命名后的文件路径传给src
                String src = "static/upload/" + newFileName;
                //写入数据库
                bookDao.add(name, author, Double.parseDouble(money), express, src);
                resp.sendRedirect("book");
                break;
            case "edit":
                id = req.getParameter("id");
                name = req.getParameter("bookname");
                author = req.getParameter("author");
                express = req.getParameter("express");
                money = req.getParameter("money");
                String url = req.getParameter("img");
                bookDao.edit(name, author, express, Double.parseDouble(money), url, Integer.parseInt(id));
                break;
            default:
                break;
        }
    }
}
