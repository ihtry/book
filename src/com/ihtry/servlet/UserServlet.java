package com.ihtry.servlet;

import com.ihtry.dao.UserDao;
import com.ihtry.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/user")
public class UserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserDao userDao = new UserDao();
        String method = req.getParameter("method");
        String id = null;
        if (method == null) {
            method = "index";
        }
        switch (method) {
            //查看用户列表调用index
            case "index":
                List<User> lists = userDao.getUser();
                req.setAttribute("lists", lists);
                req.getRequestDispatcher("user.jsp").forward(req, resp);
                break;
            //列表中修改单个用户
            case "edit":
                id = req.getParameter("id");
                User user = userDao.queryId(Integer.parseInt(id));
                req.setAttribute("lists", user);
                req.getRequestDispatcher("update.jsp").forward(req, resp);
                break;
            //删除
            case "delete":
                id = req.getParameter("id");
                userDao.delete(Integer.parseInt(id));
                resp.sendRedirect("user");
                break;
            default:
                break;
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //跨域解决
        resp.setHeader("Access-Control-Allow-Origin", "*");
        //创捷对象
        PrintWriter out = resp.getWriter();
        UserDao userDao = new UserDao();
        //响应设置
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        //获取前端数据
        String username = null;
        String password = null;
        String id = null;
        String method = req.getParameter("mtd");
        //判断入口
        switch (method) {
            case "login":
                username = req.getParameter("username");
                password = req.getParameter("password");
                boolean login = userDao.login(username, password);
                if (login) {
                    out.write(String.valueOf(login));

                } else {
                    out.write(String.valueOf(login));
                }
                break;
            case "reg":
                username = req.getParameter("username");
                password = req.getParameter("password");
                boolean reg = userDao.queryUser(username, password);
                if (reg) {
                    out.write(String.valueOf(reg));
                } else {
                    out.write(String.valueOf(reg));
                }
                break;
            case "update":
                id = req.getParameter("id");
                username = req.getParameter("username");
                password = req.getParameter("password");
                userDao.edit(Integer.parseInt(id), username, password);
                break;
            default:
                break;

        }
    }


}
