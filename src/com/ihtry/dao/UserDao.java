package com.ihtry.dao;

import com.ihtry.entity.User;
import com.ihtry.util.JdbcTool;
import com.ihtry.util.StatusTool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
    List<User> lists = new ArrayList<User>();
    //连接对象
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    /**
     * 查询全部用户
     *
     * @return 用户列表数组
     */
    public List<User> getUser() {
        JdbcTool jdbcTool = new JdbcTool();

        try {
            conn = jdbcTool.getConn();
            String sql = "select * from admin";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Integer id = rs.getInt("id");
                String username = rs.getString("username");
                String password = rs.getString("password");
                User user = new User(id, username, password);

                lists.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            jdbcTool.release(conn, ps, null);
        }
        return lists;
    }

    /**
     * 删除用户方法
     *
     * @param id id
     */
    public void delete(Integer id) {
        JdbcTool jdbcTool = new JdbcTool();

        try {
            conn = jdbcTool.getConn();
            String sql = "delete from admin where id=?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            jdbcTool.release(conn, ps, null);
        }

    }

    /**
     * 用户名查询
     *
     * @param username 用户名
     * @param password 密码
     * @return boolean
     */
    public boolean queryUser(String username, String password) {
        JdbcTool jdbcTool = new JdbcTool();
        String name = null;
        String sql = "select * from admin where username=?";
        boolean code = false;
        try {
            conn = jdbcTool.getConn();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            while (rs.next()) {
                name = rs.getString("username");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (name == null) {
            return reg(username, password);
        } else {
            return false;
        }
    }

    /**
     * id查询
     *
     * @param id id
     * @return com.ihtry.entity.User
     */
    public User queryId(Integer id) {
        User user = null;
        JdbcTool jdbcTool = new JdbcTool();

        try {
            conn = jdbcTool.getConn();
            String sql = "select * from admin where id=?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                Integer id1 = rs.getInt("id");
                String username = rs.getString("username");
                String password = rs.getString("password");
                user = new User(id1, username, password);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;

    }

    /**
     * 修改用户方法
     *
     * @param id id
     * @param username 用户名
     * @param password 密码
     */
    public void edit(Integer id, String username, String password) {
        JdbcTool jdbcTool = new JdbcTool();

        try {
            conn = jdbcTool.getConn();
            String sql = "update admin set username=?,password=? where id=?";
            ps = conn.prepareStatement(sql);
            ps.setInt(3, id);
            ps.setString(1, username);
            ps.setString(2, password);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            jdbcTool.release(conn, ps, null);
        }
    }

    /**
     * 用户登录
     *
     * @param name 用户名
     * @param pass  密码
     * @return com.ihtry.entity.User
     */
    public boolean login(String name, String pass) {
        //创建对象
        StatusTool status = new StatusTool();
        JdbcTool jdbc = new JdbcTool();
        String username = null;
        String password = null;
        String sql = "select * from admin where username=?";
        try {
            conn = jdbc.getConn();
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            rs = ps.executeQuery();
            while (rs.next()) {
                username = rs.getString("username");
                password = rs.getString("password");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            jdbc.release(conn, ps, rs);
        }
        Boolean code = false;
        //判断是否有该用户
        if (username == null && password == null) {
            code = false;
        } else {
            code = status.LoginStatus(username, password, name, pass);
        }

        return code;
    }

    /**
     * 用户注册
     *
     * @param name 用户名
     * @param pass 密码
     * @return boolean
     */
    public boolean reg(String name, String pass) {
        //创建对象
        JdbcTool jdbc = new JdbcTool();
        try {
            String sql = "insert into admin(username,password) values(?,?)";
            conn = jdbc.getConn();
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, pass);
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            jdbc.release(conn, ps, null);
        }
        return true;
    }


}
