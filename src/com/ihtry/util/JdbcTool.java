package com.ihtry.util;

//数据库连接
import java.sql.*;


public class JdbcTool {
    /**
     * @param conn 创建连接
     * @param url  数据库地址
     * @param user  数据库用户名
     * @param url  数据库密码
     */
    private Connection conn;
    private String url = "jdbc:mysql://localhost:3306/bookos?serverTimezone=UTC&useUnicode=true&characterEncoding=UTF-8";
    private String user = "root";
    private String pass = "root";


    static {
        try {
            //加载mysql驱动
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取数据库连接
     *
     * @return 返回数据库连接值
     */
    public Connection getConn() {
        try {
            conn = DriverManager.getConnection(url, user, pass);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    /**
     * 关闭数据库连接，减少资源占用
     *
     * @param conn conn
     * @param stmt stmt
     * @param rs rs
     */
    public void release(Connection conn, Statement stmt, ResultSet rs) {
        try {
            if (conn != null) {
                conn.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
