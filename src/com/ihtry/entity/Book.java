package com.ihtry.entity;
//图书类
public class Book {
    private Integer id;
    private String name;
    private String author;
    private Double money;
    private String express;
    private String img;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public String getExpress() {
        return express;
    }

    public void setExpress(String express) {
        this.express = express;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Book(Integer id, String name, String author, Double money, String express, String img) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.money = money;
        this.express = express;
        this.img = img;
    }

    public Book() {
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", money=" + money +
                ", express='" + express + '\'' +
                ", img='" + img + '\'' +
                '}';
    }
}
