<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zh_cn">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>图书管理系统-登录界面</title>
    <link rel="stylesheet" href="./static/mdui/css/mdui.min.css">
    <link rel="shortcut icon" href="./static/favicon.ico"/>
    <link rel="stylesheet" href="./static/style.css">
    <script src="./static/js/jquery.min.js"></script>
    <script src="./static/layer/layer.js"></script>
    <script src="./static/mdui/js/mdui.min.js"></script>
</head>

<body class="mdui-container mdui-theme-primary-deep-purple mdui-theme-accent-purple" style="height: 100vh;">
<div class="mdui-row">
    <div class="mdui-col-md-6 mdui-col-offset-md-3">
        <div class="login mdui-color-grey-50 mdui-shadow-3">
            <h2>后台管理系统</h2>
            <div class="mdui-textfield">
                <i class="mdui-icon material-icons">account_circle</i>
                <label class="mdui-textfield-label">用户名</label>
                <input class="mdui-textfield-input" type="text" id="username"/>
            </div>
            <div class="mdui-textfield">
                <i class="mdui-icon material-icons">lock</i>
                <label class="mdui-textfield-label">密码</label>
                <input class="mdui-textfield-input" type="password" id="password"/>
            </div>
            <div class="mdui-textfield">
                <button class="mdui-btn mdui-color-theme-500 mdui-btn-raised" type="submit" id="login"
                        style="color: #fff!important;">登录
                </button>
                <a href="reg.jsp" class="reg_href">没有账号？点击注册</a>
            </div>
        </div>
    </div>
</div>

<script src="./static/js/index.js"></script>
</body>

</html>