<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%--添加图书--%>
<jsp:include page="head.jsp"></jsp:include>
<main class="mdui-container" style="margin-top: 80px;">
    <div class="mdui-row">
        <div class="mdui-col-md-6 mdui-col-offset-md-3">
            <div class="mdui-panel" mdui-panel>
                <div class="mdui-panel-item mdui-panel-item-open">
                    <div class="mdui-panel-item-header">添加书籍</div>
                    <div class="mdui-panel-item-body">
                        <form method="post" enctype="multipart/form-data" id="addForm" action="book">
                            <div class="mdui-textfield">
                                <input type="hidden" name="method" value="add">
                            </div>
                            <div class="mdui-textfield">
                                <i class="mdui-icon material-icons">bookmark</i>
                                <label class="mdui-textfield-label">书名</label>
                                <input class="mdui-textfield-input" type="text" id="bookname" name="bookname"/>
                            </div>
                            <div class="mdui-textfield">
                                <i class="mdui-icon material-icons">account_box</i>
                                <label class="mdui-textfield-label">作者名</label>
                                <input class="mdui-textfield-input" type="text" id="author" name="author"/>
                            </div>
                            <div class="mdui-textfield">
                                <i class="mdui-icon material-icons">home</i>
                                <label class="mdui-textfield-label">出版社</label>
                                <input class="mdui-textfield-input" type="text" id="express" name="express"/>
                            </div>
                            <div class="mdui-textfield">
                                <i class="mdui-icon material-icons">attach_money</i>
                                <label class="mdui-textfield-label">价格</label>
                                <input class="mdui-textfield-input" type="text" id="money" name="money"/>
                            </div>
                            <div class="mdui-textfield">
                                <i class="mdui-icon material-icons">image</i>
                                <label class="mdui-textfield-label">封面</label>
                                <input class="mdui-textfield-input" type="file" id="img" name="img"
                                       accept="image/gif,image/jpeg,image/png"/>
                                <div class="mdui-textfield-helper mdui-text-color-red">*推荐尺寸：高300px，宽度自适应</div>
                            </div>
                            <div class="mdui-textfield">
                                <button class="mdui-btn mdui-color-theme-500 mdui-btn-raised" type="submit" id="add"
                                        style="color: #fff!important;">添加
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="./static/js/index.js"></script>
<script src="./static/mdui/js/mdui.min.js"></script>
</body>
</html>
