<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="head.jsp"></jsp:include>
<!-- 主体信息 -->
<main class="mdui-container">
    <div class="mdui-row book-item">
        <c:forEach items="${books}" var="book">
            <div class="mdui-col-md-3 mdui-col-sm-6" style="margin-bottom: 40px!important;">
                <div class="mdui-card">
                    <div class="mdui-card-media">
                        <img src="${book.img}" style="height:300px;max-height:300px!important;"/>
                        <div class="mdui-card-media-covered">
                            <div class="mdui-card-primary">
                                <div class="mdui-card-primary-title">${book.name}</div>
                                <div class="mdui-card-primary-subtitle">${book.author}</div>
                            </div>
                        </div>
                    </div>
                    <div class="mdui-card-actions">
                        <button class="mdui-btn mdui-ripple" style="color: #E65100!important;">
                            ￥${book.money}</button>
                        <button class="mdui-btn mdui-ripple">${book.express}</button>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</main>
<script src="./static/js/index.js"></script>
<script src="./static/mdui/js/mdui.min.js"></script>
</body>

</html>