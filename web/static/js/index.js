//登录

//用户名密码校验
$("#username").blur(function () {
    if ($("#username").val() == "") {
        layer.msg("用户名不能为空！", {
            icon: 2,
        });
    }
});
$("#password").blur(function () {
    if ($("#password").val() == "") {
        layer.msg("密码不能为空！", {
            icon: 2,
        });
    }
});
//发送请求
$("#login").click(function () {
    if ($("#username").val() == "" && $("#password").val() == "") {
        layer.msg("用户名密码不能为空！", {icon: 2});
    } else {
        $.ajax({
            url: "user",
            type: "post",
            data: {
                username: $("#username").val(),
                password: $("#password").val(),
                mtd: "login",
            },
            dataType: "text",
            success: function (data) {
                if (data == "true") {
                    layer.msg("登录成功", {
                        icon: 1,
                        time: 3000,
                        end: function () {
                            window.location.href = "book";
                        },
                    });
                } else {
                    layer.msg("用户名密码错误或不存在", {icon: 2});
                }

                console.log("ok");
            },
        });
    }
});

//注册
$("#user").blur(function () {
    if ($("#user").val() == "") {
        layer.msg("用户名不能为空！", {
            icon: 2,
        });
    }
});
$("#pwd").blur(function () {
    if ($("#pwd").val() == "") {
        layer.msg("密码不能为空！", {
            icon: 2,
        });
    }
});
$("#rpwd").blur(function () {
    if ($("#rpwd").val() == "") {
        layer.msg("密码不能为空！", {
            icon: 2,
        });
    }
});

$("#reg").click(function () {
    //判断是否为空
    if ($("#user").val() == "" && $("#pwd").val() == "") {
        layer.msg("请输入账号密码！", {
            icon: 2,
        });
    } else if ($("#pwd").val() != $("#rpwd").val()) {
        layer.msg("两次密码不一致！", {
            icon: 2,
        });
    } else if ($("#user").val() == "" && $("#pwd").val() == "") {
        layer.msg("输入为空！", {
            icon: 2,
        })

    } else {

        $.ajax({
            url: "user",
            type: "post",
            data: {
                username: $("#user").val(),
                password: $("#pwd").val(),
                mtd: "reg",
            },
            dataType: "text",
            success: function (data) {
                if (data == "true") {
                    var msg = layer.confirm(
                        "用户名：" + $("#user").val() + "<br/>密码：" + $("#pwd").val(),
                        {
                            btn: ["前往登录", "取消"],
                            title: "注册成功！",
                        },
                        function () {
                            window.location.href = "index.jsp";
                        }
                    );
                } else {
                    layer.msg("用户名重复!", {icon: 2})
                }


            },
        });
    }
});

//修改
$("#edit").click(function () {
    if ($("#edit_user").val() == "" && $("#edit_user").val() == "") {
        layer.msg("用户名密码不能为空", {
            icon: 2,
        });
    } else {
        $.ajax({
            url: "user",
            type: "post",

            data: {
                id: $("#edit_id").val(),
                username: $("#edit_user").val(),
                password: $("#edit_pass").val(),
                mtd: "update",
            },
            success: function () {
                layer.msg("修改成功", {
                    time: 3000,
                    end: function () {
                        window.location.href = "user";
                    },
                });
            },
        });
    }
});

//跳转user
$("#go").click(function () {
    window.location.href = "user";
});

//添加书籍

$("#bookname").blur(function () {
    if ($("#bookname").val() == "") {
        layer.msg("书名不能为空", {
            icon: 2,
        });
    }
});
$("#author").blur(function () {
    if ($("#author").val() == "") {
        layer.msg("作者名不能为空", {
            icon: 2,
        });
    }
});
$("#express").blur(function () {
    if ($("#express").val() == "") {
        layer.msg("出版社不能为空", {
            icon: 2,
        });
    }
});
$("#money").blur(function () {
    if ($("#money").val() == "") {
        layer.msg("金额不能为空", {
            icon: 2,
        });
    }
});

//更新图书
$("#edit_book").click(function () {
    if ($("#bookname").val() == "" && $("#author").val() == "" && $("#express").val() == "" && $("#money").val() == "") {
        layer.msg("输入为空", {icon: 2})
    } else {
        $.ajax({
            url: "book",
            type: "post",
            data: {
                bookname: $("#bookname").val(),
                author: $("#author").val(),
                express: $("#express").val(),
                money: $("#money").val(),
                img: $("#img").val(),
                id: $("#id").val(),
                method: "edit"
            },
            success: function () {
                layer.msg("修改成功", {
                    icon: 1,
                    time: 3000,
                    end: function () {
                        window.location.href = "book";
                    }

                })

            }


        })

    }


})



