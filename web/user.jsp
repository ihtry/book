<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--用户信息--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="head.jsp"></jsp:include>
<main class="mdui-container" style="margin-top: 80px;">
    <div class="mdui-row">
        <div class="mdui-col-md-10 mdui-col-offset-md-1">
            <div class="mdui-panel" mdui-panel>
                <div class="mdui-panel-item mdui-panel-item-open">
                    <div class="mdui-panel-item-header">用户信息</div>
                    <div class="mdui-panel-item-body">
                        <div class="mdui-table-fluid">
                            <table class="mdui-table mdui-table-hoverable">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>用户名</th>
                                    <th>密码</th>
                                    <th>操作</th>
                                </tr>
                                </thead>

                                <tbody>
                                <c:forEach items="${lists}" var="user">
                                    <tr>
                                        <td>${user.id}</td>
                                        <td>${user.username}</td>
                                        <td>${user.password}</td>
                                        <td>
                                            <a href="user?method=edit&id=${user.id}"
                                               class="mdui-btn mdui-btn-raised mdui-btn-dense mdui-color-theme-accent mdui-ripple">修改</a>
                                            <a href="user?method=delete&id=${user.id}"
                                               class="mdui-btn mdui-btn-raised mdui-btn-dense mdui-color-theme-accent mdui-ripple">删除</a>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</main>
<script src="./static/js/index.js"></script>
<script src="./static/mdui/js/mdui.min.js"></script>
</body>
</html>
