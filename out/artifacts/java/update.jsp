<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--修改用户--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="head.jsp"></jsp:include>
<main class="mdui-container" style="margin-top: 80px;">
    <div class="mdui-row">
        <div class="mdui-col-md-8 mdui-col-offset-md-2">
            <div class="mdui-panel" mdui-panel>
                <div class="mdui-panel-item mdui-panel-item-open">
                    <div class="mdui-panel-item-header">修改用户</div>
                    <div class="mdui-panel-item-body">
                        <div class="mdui-textfield">
                            <input class="mdui-textfield-input" type="hidden" name="id" value="${lists.id}" id="edit_id"/>
                        </div>
                        <div class="mdui-textfield">
                            <label class="mdui-textfield-label">用户名</label>
                            <input class="mdui-textfield-input" type="text" name="username" value="${lists.username}" id="edit_user"/>
                        </div>
                        <div class="mdui-textfield">
                            <label class="mdui-textfield-label">密码</label>
                            <input class="mdui-textfield-input" type="text" name="password" value="${lists.password}" id="edit_pass"/>
                        </div>
                        <div class="mdui-textfield">
                            <button class="mdui-btn mdui-color-theme-500 mdui-btn-raised" type="submit" id="edit" style="color: #fff!important;">修改</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="./static/js/index.js"></script>
<script src="./static/mdui/js/mdui.min.js"></script>
</body>
</html>
