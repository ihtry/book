<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="head.jsp"></jsp:include>
<!-- 主体信息 -->
<main class="mdui-container" style="margin-top: 80px;">
    <div class="mdui-row">
        <div class="mdui-col-md-10 mdui-col-offset-md-1">
            <div class="mdui-panel" mdui-panel>
                <div class="mdui-panel-item mdui-panel-item-open">
                    <div class="mdui-panel-item-header">图书列表</div>
                    <div class="mdui-panel-item-body">
                        <div class="mdui-table-fluid">
                            <table class="mdui-table mdui-table-hoverable">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>书名</th>
                                    <th>作者名</th>
                                    <th>出版社</th>
                                    <th>金额</th>
                                    <th>封面</th>
                                    <th>操作</th>
                                </tr>
                                </thead>

                                <tbody>
                                <c:forEach items="${books}" var="book">
                                    <tr>
                                        <td>${book.id}</td>
                                        <td>${book.name}</td>
                                        <td>${book.author}</td>
                                        <td>${book.express}</td>
                                        <td>${book.money}</td>
                                        <td>
                                            <div class="mdui-dialog" id="showDialog">
                                                <div class="mdui-dialog-content">
                                                    <img src="${book.img}" alt="${book.name}" style="margin: 0 auto;">
                                                </div>
                                                <div class="mdui-dialog-actions">
                                                    <button class="mdui-btn mdui-ripple" mdui-dialog-close>关闭</button>
                                                </div>
                                            </div>
                                            <a href="javascript:void(0)"
                                               class="mdui-text-color-blue mdui-ripple"
                                               mdui-dialog="{target:'#showDialog'}">预览</a>
                                        </td>
                                        <td>
                                            <a href="book?method=edit&id=${book.id}"
                                               class="mdui-btn mdui-btn-raised mdui-btn-dense mdui-color-theme-accent mdui-ripple">修改</a>
                                            <a href="book?method=delete&id=${book.id}"
                                               class="mdui-btn mdui-btn-raised mdui-btn-dense mdui-color-theme-accent mdui-ripple">删除</a>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="./static/js/index.js"></script>
<script src="./static/mdui/js/mdui.min.js"></script>
</body>
</html>
