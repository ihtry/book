<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>图书管理系统</title>
    <link rel="shortcut icon" href="./static/favicon.ico"/>
    <link rel="stylesheet" href="./static/mdui/css/mdui.min.css">
    <link rel="stylesheet" href="./static/style.css">
    <script src="./static/js/jquery.min.js"></script>
    <script src="./static/layer/layer.js"></script>
</head>

<body class="mdui-theme-primary-deep-purple mdui-theme-accent-purple">
<!-- 导航 -->
<header class="mdui-appbar mdui-appbar-inset mdui-color-theme mdui-appbar-fixed mdui-appbar-scroll-hide appbar">
    <div class="mdui-toolbar">
            <span class="mdui-btn mdui-btn-icon mdui-ripple mdui-ripple-white"
                  mdui-drawer="{target: '#main-drawer', swipe: true}">
                <i class="mdui-icon material-icons">menu</i>
            </span>
        <a href="javascript:;" class="mdui-typo-headline">图书管理系统</a>
        <div class="mdui-toolbar-spacer">
        </div>
        <div id="go" target="_blank" class="mdui-btn mdui-btn-icon mdui-ripple mdui-ripple-white"
             mdui-tooltip="{content: '用户管理'}">
            <i class="mdui-icon material-icons">supervisor_account</i>
        </div>
    </div>
</header>
<!-- 侧边导航 -->
<div class="mdui-drawer mdui-drawer-close mdui-drawer-full-height" id="main-drawer">
    <div class="mdui-list" mdui-collapse="{accordion: true}" style="margin-bottom: 76px;">
        <div class="mdui-collapse-item">
            <a href="book" class="mdui-list-item mdui-ripple">首页</a>
            <a href="book?method=list" class="mdui-list-item mdui-ripple">图书管理</a>
            <a href="add.jsp" class="mdui-list-item mdui-ripple">添加图书</a>
        </div>

    </div>
</div>

