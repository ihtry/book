<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--编辑图书--%>
<jsp:include page="head.jsp"></jsp:include>
<main class="mdui-container" style="margin-top: 80px;">
    <div class="mdui-row">
        <div class="mdui-col-md-6 mdui-col-offset-md-3">
            <div class="mdui-panel" mdui-panel>
                <div class="mdui-panel-item mdui-panel-item-open">
                    <div class="mdui-panel-item-header">编辑图书</div>
                    <div class="mdui-panel-item-body">
                        <div class="mdui-textfield">
                            <input type="hidden" name="method" value="${book.id}" id="id">
                        </div>
                        <div class="mdui-textfield">
                            <i class="mdui-icon material-icons">bookmark</i>
                            <label class="mdui-textfield-label">书名</label>
                            <input class="mdui-textfield-input" type="text" id="bookname" name="bookname"
                                   value="${book.name}"/>
                        </div>
                        <div class="mdui-textfield">
                            <i class="mdui-icon material-icons">account_box</i>
                            <label class="mdui-textfield-label">作者名</label>
                            <input class="mdui-textfield-input" type="text" id="author" name="author"
                                   value="${book.author}"/>
                        </div>
                        <div class="mdui-textfield">
                            <i class="mdui-icon material-icons">home</i>
                            <label class="mdui-textfield-label">出版社</label>
                            <input class="mdui-textfield-input" type="text" id="express" name="express"
                                   value="${book.express}"/>
                        </div>
                        <div class="mdui-textfield">
                            <i class="mdui-icon material-icons">attach_money</i>
                            <label class="mdui-textfield-label">价格</label>
                            <input class="mdui-textfield-input" type="text" id="money" name="money"
                                   value="${book.money}"/>
                        </div>
                        <div class="mdui-textfield">
                            <i class="mdui-icon material-icons">image</i>
                            <label class="mdui-textfield-label">封面</label>
                            <input class="mdui-textfield-input" type="text" id="img" name="img" value="${book.img}" disabled/>
                            <div class="mdui-textfield-helper mdui-text-color-red">* 暂不支持修改,如需修改请删除重新添加</div>
                        </div>
                        <div class="mdui-textfield">
                            <button class="mdui-btn mdui-color-theme-500 mdui-btn-raised" type="submit" id="edit_book"
                                    style="color: #fff!important;">修改
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="./static/js/index.js"></script>
<script src="./static/mdui/js/mdui.min.js"></script>
</body>
</html>
